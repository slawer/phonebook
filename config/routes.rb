Rails.application.routes.draw do
  root 'home#index'
  devise_for :users



  # get "disable/:id" => "contacts#disable",as: :disable
  # get "enable/:id" => "contacts#enable",as: :enable

  get 'disable_contact/:id' => "contacts#disable_contact",as: :disable_contact
  get 'enable_contact/:id' => "contacts#enable_contact",as: :enable_contact

  get 'disable_region/:id' => "regions#disable_region",as: :disable_region
  get 'enable_region/:id' => "regions#enable_region",as: :enable_region

  get 'cities/index/:region_city_id/disable_city/:id' => "cities#disable_city",as: :disable_city
  get 'cities/index/:region_city_id/enable_city/:id' => "cities#enable_city",as: :enable_city

  get 'suburbs/index/:city_suburb_id/disable_suburb/:id' => "suburbs#disable_suburb",as: :disable_suburb
  get 'suburbs/index/:city_suburb_id/enable_suburb/:id' => "suburbs#enable_suburb",as: :enable_suburb

  get 'localities/index/:suburb_locality_id/disable_locality/:id' => "localities#disable_locality",as: :disable_locality
  get 'localities/index/:suburb_locality_id/enable_locality/:id' => "localities#enable_locality",as: :enable_locality


  get 'get_region' => "contacts#get_region"
  get 'get_city' => "contacts#get_city"
  get 'get_suburb' => "contacts#get_suburb"
  get 'update_city' => "suburbs#update_city"
  get 'update_suburb' => "localities#update_suburb"
  get 'update_city_loc' => "localities#update_city_loc"


  get 'cities/index/:region_city_id' => "cities#index", :as => "city_index"

  get 'suburbs/index/:city_suburb_id' => "suburbs#index", :as => "suburb_index"

  get 'localities/index/:suburb_locality_id' => "localities#index", :as => "locality_index"



  resources :localities
  resources :suburbs
  resources :cities
  resources :regions
  resources :contacts
  resources :users
  resources :permissions_roles
  resources :permissions
  resources :roles



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


end
