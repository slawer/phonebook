class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :edit, :update, :destroy, :disable_contact, :enable_contact]
   before_action :authenticate_user!
   load_and_authorize_resource
  before_action :load_permissions

  # GET /contacts
  # GET /contacts.json
  def index
    #@contacts = Contact.all.paginate(per_page: 10, page: params[:page]).order('surname asc')

    if params[:filter_main].present?
      filter_params = params[:filter_main]

      @surname = filter_params["surname"]
      @phone_number = filter_params["phone_number"]
      @from_date = filter_params["from_date"]
      @to_date = filter_params["to_date"]

      the_search = ""

      if !@surname.empty?
        the_search = the_search + "surname ilike '%#{@surname}%' or othernames ilike '%#{@surname}%' and "
      end

      if !@phone_number.empty?
        the_search = the_search + "phone_number ilike '%#{@phone_number}%' and "
      end

      if !@from_date.empty?
        if !@to_date.empty?

          @from_date=@from_date+" 00:00:00 UTC"
          @to_date=@to_date+" 23:59:59 UTC"
          the_search = the_search + "created_at between '%#{@from_date}%' and '%#{@to_date}%' and "
        end
      end
      if the_search.empty?
        @contacts = Contact.all.paginate(per_page: 10, page: params[:page]).order('surname asc')
        logger.info "######"
        logger.info "The parameter is #{@contacts}"
        logger.info "######"
      else
        the_search=the_search+ "created_at is not null"
        @contacts=Contact.where(the_search)
        logger.info "######"
        logger.info "The parameter is #{@contacts}"
        logger.info "######"
      end

    else
      @contacts = Contact.all.paginate(per_page: 10, page: params[:page]).order('surname asc')
      logger.info "######"
      logger.info "The parameter is #{@contacts}"
      logger.info "######"

    end



    # @user = User.select("email").where(id: current_user.id)

    # demo= Contact.mysearch
    # logger.info"======= "+demo.inspect
    # if params[:search_value] && params[:search_value].strip != ''
    #   if params[:search_param] == 'firstname'
    #     @contacts = Contact.firstname_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
    #   elsif params[:search_param] == 'phone_number'
    #     logger.info "We are INSIDE THE NUMBER PARAM"
    #     @contacts = Contact.phone_number_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
    #   elsif params[:search_param] == 'date'
    #     start = (params["start_date"] + " " + "0:00:00") # Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
    #     ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"
    #     @contacts =
    #         Contact.search_date(start, ended).paginate(page: params[:page], per_page: params[:count]).order('id asc')
    #
    #   else
    #     @contacts = Contact.paginate(page: params[:page], per_page: params[:count]).order('id desc')
    #     @search_json = []
    #   end
    # end
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
    @user = User.all
  end

  # GET /contacts/new
  def new
    @contact = Contact.new

    @city = City.all.order('name asc')
    @region = Region.all.order('name asc')
    @suburb = Suburb.all.order('name asc')

    @localities = Locality.order(:name).where("name ilike ?", "%#{params[:locality_id]}")

    @loc_list = @localities.map {|a| [a.name + " ", a.id]}
  end

  # GET /contacts/1/edit
  def edit
    @city = City.all.order('name asc')
    @region = Region.all.order('name asc')
    @suburb = Suburb.all.order('name asc')
    @localities = Locality.order(:name).where("name ilike ?", "%#{params[:locality_id]}")

    @loc_list = @localities.map {|a| [a.name + " ", a.id]}
  end

  def disable_contact

    Contact.update(@contact.id, active_status: false)
    respond_to do |format|
      format.html {redirect_to contacts_url, notice: 'Contact was successfully disabled.'}
      format.json {head :no_content}
    end
  end

  def enable_contact

    Contact.update(@contact.id, active_status: true)
    respond_to do |format|
      format.html {redirect_to contacts_url, notice: 'Contact was successfully enabled.'}
      format.json {head :no_content}
    end
  end

  def get_region
    region_id = params["region_id"]
    @city_data = City.where(region_id: region_id).order('name asc')

    @city_data = @city_data.map {|a| [a.name, a.id]}.insert(0, "")
  end

  def get_city
    city_id = params["city_id"]
    @suburb_data = Suburb.where(city_id: city_id).order('name asc')

    @suburb_data = @suburb_data.map {|a| [a.name, a.id]}.insert(0, "")
  end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        format.html {redirect_to @contact, notice: 'Contact was successfully created.'}
        format.json {render :show, status: :created, location: @contact}
      else
        format.html {render :new}
        format.json {render json: @contact.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    respond_to do |format|
      if @contact.update(contact_params)
        format.html {redirect_to @contact, notice: 'Contact was successfully updated.'}
        format.json {render :show, status: :ok, location: @contact}
      else
        format.html {render :edit}
        format.json {render json: @contact.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact.destroy
    respond_to do |format|
      format.html {redirect_to contacts_url, notice: 'Contact was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_contact
    @contact = Contact.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def contact_params
    params.require(:contact).permit(:surname, :othernames, :phone_number, :user_id, :active_status, :del_status, :locality_id)
  end
end
