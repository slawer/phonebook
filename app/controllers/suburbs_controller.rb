class SuburbsController < ApplicationController
  before_action :set_suburb, only: [:show, :edit, :update, :destroy,:disable_suburb,:enable_suburb]
  before_action :authenticate_user!
   load_and_authorize_resource
  before_action :load_permissions

  # GET /suburbs
  # GET /suburbs.json
  def index
    @suburbs = Suburb.where(city_id: params[:city_suburb_id]).order('name asc')
    @city = City.find(params[:city_suburb_id])
  end

  # GET /suburbs/1
  # GET /suburbs/1.json
  def show
  end

  # GET /suburbs/new
  def new
    @suburb = Suburb.new
    @region = Region.all
  end

  # GET /suburbs/1/edit
  def edit
    @region = Region.all
  end

  def disable_suburb
    #params[:reg_id] = city_params[:region_city_id]
    Suburb.update(@suburb.id,status: false)
    respond_to do |format|
      format.html { redirect_to suburb_index_path(params[:city_suburb_id]), notice: 'Suburb was successfully disabled.' }
      format.json { head :no_content }
    end
  end

  def enable_suburb
    #params[:reg_id] = city_params[:region_city_id]
    Suburb.update(@suburb.id,status: true)
    respond_to do |format|
      format.html { redirect_to suburb_index_path(params[:city_suburb_id]), notice: 'Suburb was successfully enabled.' }
      format.json { head :no_content }
    end
  end

  # POST /suburbs
  # POST /suburbs.json
  def create
    @suburb = Suburb.new(suburb_params)
    params[:cit_id] = suburb_params[:city_id]
    logger.info "This is city ID:: #{params[:cit_id].inspect}"

    respond_to do |format|
      if @suburb.save
        format.html { redirect_to @suburb, notice: 'Suburb was successfully created.' }
        format.json { render :show, status: :created, location: @suburb }
      else
        format.html { render :new }
        format.json { render json: @suburb.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /suburbs/1
  # PATCH/PUT /suburbs/1.json
  def update
    respond_to do |format|
      params[:cit_id] = suburb_params[:city_id]
      logger.info "This is city ID:: #{params[:cit_id].inspect}"
      if @suburb.update(suburb_params)
        format.html { redirect_to @suburb, notice: 'Suburb was successfully updated.' }
        format.json { render :show, status: :ok, location: @suburb }
      else
        format.html { render :edit }
        format.json { render json: @suburb.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_city
    reg_id=params["region_id"]
    @city_data=City.where(region_id: reg_id)

    @city_data=@city_data.map{|a|[a.name,a.id]}.insert(0,"Please select from the option below")

    puts "----------------------------------"
    puts @city_data.inspect
    puts "----------------------------------"

  end

  # DELETE /suburbs/1
  # DELETE /suburbs/1.json
  def destroy
    @suburb.destroy
    respond_to do |format|
      format.html { redirect_to suburbs_url, notice: 'Suburb was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_suburb
      @suburb = Suburb.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def suburb_params
      params.require(:suburb).permit(:name, :desc, :status, :city_id)
    end
end
