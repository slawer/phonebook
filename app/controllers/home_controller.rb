class HomeController < ApplicationController

  def index
    @roles = Role.all
    @regions = Region.all.order('name asc')
  end
end
