class LocalitiesController < ApplicationController
  before_action :set_locality, only: [:show, :edit, :update, :destroy, :disable_locality, :enable_locality]
  before_action :authenticate_user!
   load_and_authorize_resource
  before_action :load_permissions

  # GET /localities
  # GET /localities.json
  def index
    @localities = Locality.where(suburb_id: params[:suburb_locality_id]).order('name asc')
    @suburb = Suburb.find(params[:suburb_locality_id])
  end

  # GET /localities/1
  # GET /localities/1.json
  def show
  end

  # GET /localities/new
  def new
    @locality = Locality.new
    @city = City.all
    @region = Region.all
  end

  # GET /localities/1/edit
  def edit
    @city = City.all
    @region = Region.all
  end

  def update_city_loc
    reg_id = params["region_id"]
    @city_data = City.where(region_id: reg_id)

    @city_data = @city_data.map {|a| [a.name, a.id]}.insert(0, "")

    puts "----------------------------------"
    puts @city_data.inspect
    puts "----------------------------------"

  end

  def update_suburb

    city_id = params["city_id"]
    @suburb_data = Suburb.where(city_id: city_id)

    @suburb_data = @suburb_data.map {|a| [a.name, a.id]}.insert(0, "")

    puts "----------------------------------"
    puts "----------------------------------"
    puts @suburb_data.inspect
    puts "----------------------------------"
    puts "----------------------------------"
  end

  def disable_locality
    Locality.update(@locality.id, status: false)

    respond_to do |format|
      format.html {redirect_to locality_index_path(params[:suburb_locality_id]), notice: 'Locality was successfully disabled.'}
      format.json {head :no_content}
    end
  end

  def enable_locality
    Locality.update(@locality.id, status: true)

    respond_to do |format|
      format.html {redirect_to locality_index_path(params[:suburb_locality_id]), notice: 'Locality was successfully enabled.'}
      format.json {head :no_content}
    end
  end

  # POST /localities
  # POST /localities.json
  def create
    @locality = Locality.new(locality_params)

    params[:sub_id] = locality_params[:suburb_id]
    logger.info "This is suburb ID:: #{params[:sub_id].inspect}"
    respond_to do |format|
      if @locality.save
        format.html {redirect_to @locality, notice: 'Locality was successfully created.'}
        format.json {render :show, status: :created, location: @locality}
      else
        format.html {render :new}
        format.json {render json: @locality.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /localities/1
  # PATCH/PUT /localities/1.json
  def update
    respond_to do |format|
      params[:sub_id] = locality_params[:suburb_id]
      logger.info "This is suburb ID:: #{params[:sub_id].inspect}"
      if @locality.update(locality_params)
        format.html {redirect_to @locality, notice: 'Locality was successfully updated.'}
        format.json {render :show, status: :ok, location: @locality}
      else
        format.html {render :edit}
        format.json {render json: @locality.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /localities/1
  # DELETE /localities/1.json
  def destroy
    @locality.destroy
    respond_to do |format|
      format.html {redirect_to localities_url, notice: 'Locality was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_locality
    @locality = Locality.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def locality_params
    params.require(:locality).permit(:name, :desc, :status, :suburb_id)
  end
end
