class Suburb < ApplicationRecord
  belongs_to :city,foreign_key: :city_id, class_name: "City"

  has_many :localities, class_name: "Locality"

  validates :name, presence: {message: "Suburb name cannot be blank"}
  validates :desc, presence: {message: "Description cannot be blank"}
end
