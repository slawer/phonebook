class Region < ApplicationRecord
  has_many :cities, class_name: "City"

  validates :name, presence: {message: "Region name cannot be blank"}
  validates :desc, presence: {message: "Description cannot be blank"}
end
