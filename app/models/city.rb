class City < ApplicationRecord
  belongs_to :region,foreign_key: :region_id, class_name: "Region"

  # belongs_to :contact,foreign_key: :region_id, class_name: "Contact"

  has_many :suburbs, class_name: "Suburb"

  validates :name, presence: {message: "City name cannot be blank"}
  validates :desc, presence: {message: "Description cannot be blank"}
end
