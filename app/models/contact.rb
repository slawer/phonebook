class Contact < ApplicationRecord
  belongs_to :user, foreign_key: :user_id, class_name: "User"

  belongs_to :locality, foreign_key: :locality_id, class_name: "Locality"

  validates :phone_number, presence: {message: "Phone Number cannot be blank"}, length: {minimum: 10, maximum: 10},uniqueness: true
  validates :surname, presence: {message: "Surname cannot be blank"}
  validates :othernames, presence: {message: "Othername(s) cannot be blank"}

  def self.firstname_search(firstname)
    firstname = "%#{firstname}%"
    where('surname LIKE ?', firstname)
  end

  def self.phone_number_search(phone_number)
    phone_number = "%#{phone_number}%"
    where('phone_number LIKE ?', phone_number)
  end


  def surname=(value)
    super value.titleize
  end

  def self.mysearch
    where(id: 1)
  end
end
