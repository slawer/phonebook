class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name,presence: true
  validates :email,presence: true
  belongs_to :role,class_name: "Role",foreign_key: :role_id

  def super_admin?
    self.role.name=="Super Admin"
  end
end
