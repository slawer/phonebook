class Locality < ApplicationRecord
  belongs_to :suburb,foreign_key: :suburb_id, class_name: "Suburb"

  validates :name, presence: {message: "Locality name cannot be blank"}
  validates :desc, presence: {message: "Description cannot be blank"}
end
