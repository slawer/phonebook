json.extract! suburb, :id, :name, :desc, :status, :city_id, :created_at, :updated_at
json.url suburb_url(suburb, format: :json)
