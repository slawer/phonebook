json.extract! region, :id, :name, :desc, :status, :created_at, :updated_at
json.url region_url(region, format: :json)
