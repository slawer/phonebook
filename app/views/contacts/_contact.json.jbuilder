json.extract! contact, :id, :surname, :othernames, :phone_number, :user_id, :active_status, :del_status, :created_at, :updated_at
json.url contact_url(contact, format: :json)
