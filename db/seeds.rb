# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#the highest role with all the permissions.
#Role.create(name: 'Super Admin')
#Role.create(name: 'Staff')

Permission.create(action: 'manage', subject_class:'all')

# create a user and assign the super admin role to him
#User.create(name: "Albert Lawer", email: "albert@albertlawer.com", password: "albert", password_confirmation: "albert", role_id: Role.find_by_name('Super Admin').id)

#User.create(name: "Neo", email: "neo@matrix.com", password: "the_one", password_confirmation: "the_one", role_id: Role.find_by_name('Staff').id)