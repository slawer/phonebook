class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.string :surname
      t.string :othernames
      t.string :phone_number
      t.integer :user_id
      t.integer :locality_id
      t.boolean :active_status
      t.boolean :del_status

      t.timestamps
    end
  end
end
