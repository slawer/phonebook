class CreateSuburbs < ActiveRecord::Migration[5.1]
  def change
    create_table :suburbs do |t|
      t.string :name
      t.string :desc
      t.boolean :status
      t.integer :city_id

      t.timestamps
    end
  end
end
